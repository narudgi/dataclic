using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Rotation : MonoBehaviour {

	public bool world;
	public Vector3 rotation;

	void LateUpdate () {
		if(world)
			transform.Rotate (rotation*Time.deltaTime*70, Space.World);
		else 
			transform.Rotate (rotation*Time.deltaTime*70, Space.Self);
	}
}
