using System;

namespace Game
{
	[Serializable]
	public struct Timer
	{
		public float endValue;

		public float Value { get; private set; }
		public bool IsComplete { get; private set; }
		public delegate void OnComplete();
		public OnComplete OnCompleted { get; private set; }

		public Timer(float timer)
		{
			endValue = timer;
			Value = 0f;
			IsComplete = true;
			OnCompleted = default;
		}

		public void Assign(OnComplete onComplete)
		{
			OnCompleted = onComplete;
		}

		public void Start(float timer)
		{
			endValue = timer;
			Value = 0f;
			IsComplete = false;
		}

		public void Tick(float value)
		{
			if (IsComplete)
				return;
			if (Value >= endValue)
			{
				Value = endValue;
				IsComplete = true;
				OnCompleted?.Invoke();
				return;
			}

			Value += value;
		}
	}
}