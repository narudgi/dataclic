using UnityAtoms;
using UnityEngine;

namespace StateMachine.Atoms
{
	public class TimerTransition : StateMachineBehaviour
	{
		[SerializeField] FloatReference duration = default;
		[SerializeField] string stateName = string.Empty;

		float timer = 0f;

		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			StartTimer(duration);
		}

		override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (timer <= 0)
			{
				timer = 0;
				animator.Play(stateName);
				return;
			}
			timer -= Time.deltaTime;
		}

		void StartTimer(float duration)
		{
			timer = duration;
		}
	}
}