using UnityEngine;

namespace Game
{
    public class StateEvents : MonoBehaviour
    {
        [SerializeField] StateEvent[] stateEvents = default;

        public void TriggerEvent(string name)
        {
            foreach (var stateEvent in stateEvents)
                if (stateEvent.name.Equals(name))
                    stateEvent.reponse?.Invoke();
        }
    }
}